package com.mx.intellego.zurich.ecm;

import com.box.sdk.BoxAPIConnection;
import com.box.sdk.BoxFile;
import com.box.sdk.Metadata;

public class MetaDato {

	// Crear Metadato

	public void createMetadata(String tokenConnection, String id) {

		try {
			System.out.println("Ingresa metodo crear MetaDato");
			Metadata metadata, metadata2;
			metadata2 =metadata = new Metadata();
            metadata.add("/atributo2", "valor1");
			System.out.println("Valor de la metadata:" + metadata2.get("/atributo2"));

			BoxAPIConnection api = new BoxAPIConnection(tokenConnection);
			BoxFile file = new BoxFile(api, id);
			file.createMetadata(metadata);
			metadata2 = file.getMetadata();

			System.out.println("Valor de la metadata:" + metadata2.get("/atributo2"));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// Subir Metadato
	public void updateMetadata(String tokenConnection, String id) {
		
		

		System.out.println("Ingresa metodo actualizar MetaDato");
		Metadata metadata;
		
		BoxAPIConnection api = new BoxAPIConnection(tokenConnection);
		BoxFile file = new BoxFile(api, id);
		metadata = file.getMetadata();
		metadata.replace("/atributo2", "valor2");
		file.updateMetadata(metadata);
	}

	// Obtener un Metadato

	public Metadata getMetadata(String tokenConnection, String id) {
		System.out.println("Ingresa metodo obtener MetaDato");
		BoxAPIConnection api = new BoxAPIConnection(tokenConnection);
		BoxFile file = new BoxFile(api, id);
		System.out.println("Valor de la metadata" + file.getMetadata().get("/atributo2"));
		return file.getMetadata();

	}

}
