package com.mx.intellego.zurich.ecm;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import com.box.sdk.BoxAPIConnection;
import com.box.sdk.BoxFile;
import com.box.sdk.BoxFileVersion;


public class Descargar {

	
 public void OutputStreamdownload(String tokenConnection, String idDocumento) throws IOException {
		OutputStream output;
           System.out.println("Inicio del metodo descarga el archivo");
		
       	BoxAPIConnection api = new BoxAPIConnection(tokenConnection);
		BoxFile file = new BoxFile(api , idDocumento);
		BoxFile.Info info = file.getInfo();

		FileOutputStream stream = new FileOutputStream(info.getName());
		file.download(stream);
		System.out.println("Se descargo el archivo");
		stream.close();
		
		
	}
		
		
}
